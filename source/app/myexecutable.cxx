// Local include(s):
#include "mylibrary.h"
#include "baseClass.h"
#include "derClass.h"
#include <iostream>

int main() {

    basic_class* myclass = new basic_class(3,4);
    derived_class* newclass = new derived_class(3,4);
    
    int output = myclass->my_function();    // should be 7
    int output2 = newclass->my_function();  // should be -1
    
    std::cout << "basic class output: " << output << std::endl;
    std::cout << "derived class output: " << output2 << std::endl;
    
    // upto this point, works as expected
    
    // array declared
    basic_class array_basic[3];
    
    array_basic[0] =  *(new basic_class(3,4) );
    array_basic[1] =  *(new derived_class(3,4));
    
    std::cout << "basic class output: " << array_basic[0].my_function() << std::endl;   //should be 7
    std::cout << "derived class output: " << array_basic[1].my_function() << std::endl; //should be -1
    
    
    const auto& auto_def_class = new derived_class(3,4);
    array_basic[2] = *(auto_def_class);
    
    std::cout << "auto derived class output: " << array_basic[2].my_function() << std::endl; //should be -1

    /*

     the following doesn't compile, it would if inheritance behaviour was like 20.7
     
     std::cout << "auto derived class output: " << array_basic[2].my_other_function() << std::endl;
    
    */
     
    // Return gracefully:
    return 0;
}
