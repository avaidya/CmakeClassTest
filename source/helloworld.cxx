// System include(s):
#include <iostream>

int main() {

  // Greet the world:
  std::cout << "Hello World!" << std::endl;

  // Return gracefully:
  return 0;
}
