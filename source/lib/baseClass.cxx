//includes

#include "baseClass.h"

basic_class :: basic_class (int a, int b) {
  
  first = a;
  second = b;

}


void basic_class :: assign_values (int x, int y){

  first = x;
  second = y;

}


int basic_class :: my_function () {

  int out = first + second;
  return out;

}
