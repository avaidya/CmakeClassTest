// System include(s):
#include <iostream>

// Local include(s):
#include "mylibrary.h"

/// Implement the simple function
void simpleFunction() {

  // Greet the world:
  std::cout << "Hello World from simpleFunction!" << std::endl;

  // Return gracefully:
  return;
}
