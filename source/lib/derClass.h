// Dear emacs, this is -*- c++ -*-
#ifndef LIB_DERCLASS_H
#define LIB_DERCLASS_H

#include "baseClass.h"

class derived_class : public basic_class {
    
  public:
    derived_class( int=0, int=0 );
    int my_other_function ();
    int my_function ();

};

#endif // LIB_BASECLASS_H
