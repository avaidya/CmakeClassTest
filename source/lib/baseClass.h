// Dear emacs, this is -*- c++ -*-
#ifndef LIB_BASECLASS_H
#define LIB_BASECLASS_H

class basic_class {

  protected:
    int first, second;
  public:
    void assign_values ( int, int );
    basic_class( int=0, int=0 );
    int my_function ();


};

#endif // LIB_BASECLASS_H
