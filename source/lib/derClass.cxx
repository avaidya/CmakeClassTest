#include "derClass.h"


derived_class :: derived_class  (int a, int b) {
    
    first = a;
    second = b;
    
}


int derived_class :: my_other_function () {
    
    int out = first * second;
    return out;
    
}


int derived_class :: my_function () {
    
    int out = first - second;
    return out;
    
}